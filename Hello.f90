program MPIIO_helloworld
    use mpi
    implicit none
    integer(mpi_offset_kind) :: offset
    integer, dimension(mpi_status_size) :: wstatus
    integer, parameter :: msgsize=6
    real*8,dimension(:) :: arr(msgsize)
    real*8 :: number
    character(msgsize) :: message
    integer :: ierr, rank, comsize, fileno
    integer i

    call MPI_Init(ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, comsize, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)
    
    do i=1,msgsize
         arr(i)=rank*10+i
    enddo

    if (mod(rank,2) == 0) then
        message = "Hello "
    else
        message = "World!"
    endif

    !write(500+rank,*) arr(:)
    offset = rank*8
    

    if (rank==0) number=arr(1)
    if (rank==1) number=arr(2)
    if (rank>1) number=arr(3)


    !offset = rank*msgsize

    call MPI_File_open(MPI_COMM_WORLD, "helloworld.txt", &
                       ior(MPI_MODE_CREATE,MPI_MODE_WRONLY),&
                       MPI_INFO_NULL, fileno, ierr)
 
    !call MPI_File_seek (fileno, offset, MPI_SEEK_SET, ierr)
    !call MPI_File_write(fileno, number, 1, MPI_DOUBLE_PRECISION, &
    !                    wstatus, ierr)

     !call MPI_File_write_at_all(fileno, offset, message, msgsize, &
     !                          MPI_CHARACTER, wstatus, ierr)

         call MPI_File_write_at_all(fileno, offset, number, 1, &
                               MPI_DOUBLE_PRECISION, wstatus, ierr)

    call MPI_File_close(fileno, ierr)
    call MPI_Finalize(ierr)
end program MPIIO_helloworld


